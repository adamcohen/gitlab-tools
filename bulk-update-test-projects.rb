#!/usr/bin/env ruby

require 'rubygems'
require 'open-uri'
require 'gitlab'
require "base64"
require 'net/http'
require 'uri'

projects = [
    # { "path" => "adamcohen/create-mr-api-test", "branch" => "main" },
    # { "path" => "gitlab-org/security-products/tests/java-maven", "branch" => "update-expectation-maven-cli-opts-skip-tests-FREEZE" },
    # { "path" => "gitlab-org/security-products/tests/java-maven", "branch" => "offline-FREEZE" },
    # { "path" => "gitlab-org/security-products/tests/java-maven", "branch" => "maven-cli-opts-skip-tests-FREEZE" },
    # { "path" => "gitlab-org/security-products/tests/java-maven", "branch" => "master" },
    # { "path" => "gitlab-org/security-products/tests/java-maven", "branch" => "custom-ca-cert-java-8-FREEZE" },
    # { "path" => "gitlab-org/security-products/tests/python-pip", "branch" => "offline-FREEZE" },
    # { "path" => "gitlab-org/security-products/tests/python-pip", "branch" => "master" },
    # { "path" => "gitlab-org/security-products/tests/java-maven-multimodules", "branch" => "semgrep-migration-FREEZE" },
    # { "path" => "gitlab-org/security-products/tests/java-maven-multimodules", "branch" => "offline-FREEZE" },
    # { "path" => "gitlab-org/security-products/tests/java-maven-multimodules", "branch" => "master" },
    # { "path" => "gitlab-org/security-products/tests/scala-sbt", "branch" => "sbt-cli-opts-FREEZE" },
    # { "path" => "gitlab-org/security-products/tests/scala-sbt", "branch" => "offline-FREEZE" },
    # { "path" => "gitlab-org/security-products/tests/scala-sbt", "branch" => "master" },
    # { "path" => "gitlab-org/security-products/tests/java-gradle", "branch" => "offline-FREEZE" },
    # { "path" => "gitlab-org/security-products/tests/java-gradle", "branch" => "master" },
    # { "path" => "gitlab-org/security-products/tests/java-gradle", "branch" => "gradle-cli-opts-FREEZE" },
    # { "path" => "gitlab-org/security-products/tests/python-pipenv", "branch" => "require-system-python-FREEZE" },
    # { "path" => "gitlab-org/security-products/tests/python-pipenv", "branch" => "pipfile-lock-FREEZE" },
    # { "path" => "gitlab-org/security-products/tests/python-pipenv", "branch" => "offline-FREEZE" },
    # { "path" => "gitlab-org/security-products/tests/python-pipenv", "branch" => "master" },
    # { "path" => "gitlab-org/security-products/tests/java-gradle-multimodules", "branch" => "subprojects-buildfilename-FREEZE" },
    # { "path" => "gitlab-org/security-products/tests/java-gradle-multimodules", "branch" => "offline-FREEZE" },
    # { "path" => "gitlab-org/security-products/tests/java-gradle-multimodules", "branch" => "no-root-dependencies-FREEZE" },
    # { "path" => "gitlab-org/security-products/tests/java-gradle-multimodules", "branch" => "master" },
    # { "path" => "gitlab-org/security-products/tests/java-gradle-kotlin-dsl", "branch" => "offline-FREEZE" },
    # { "path" => "gitlab-org/security-products/tests/java-gradle-kotlin-dsl", "branch" => "master" },
    # { "path" => "gitlab-org/security-products/tests/scala-sbt-multiproject", "branch" => "main" },
    # { "path" => "gitlab-org/security-products/tests/python-setuptools", "branch" => "offline-FREEZE" },
    # { "path" => "gitlab-org/security-products/tests/python-setuptools", "branch" => "main" }
  ]

unless File.exists?(".gitlab-access-token")
  puts "Error: you must provide a GitLab private token."
  exit 1
end

# fetch project id using the API. Could also just scrape the page, but that won't allow us
# to fetch confidential issues
def fetch_project_id(project_name)
  project_path = CGI::escape(project_name)
  url = "https://gitlab.com/api/v4/projects/#{project_path}"

  project_details = JSON.parse(URI.open(url).read)
  project_details["id"]
end

private_token = File.read(".gitlab-access-token").strip
Gitlab.configure do |config|
 # API endpoint URL, default: ENV["GITLAB_API_ENDPOINT"] and falls back to ENV["CI_API_V4_URL"]
  config.endpoint       = "https://gitlab.com/api/v4"
  # user"s private token or OAuth2 access token, default: ENV["GITLAB_API_PRIVATE_TOKEN"]
  config.private_token  = private_token
end

projects.each do |project|
  puts %|Processing project #{project["path"]} with branch #{project["branch"]}|

  project_id = fetch_project_id(project["path"])
  ref = project["branch"]

  filename = ".gitlab-ci.yml"
  gitlab_ci = Gitlab.get_file(project_id, filename, ref)

  content = Base64.decode64(gitlab_ci.content)

  modified_contents = ""
  gemnasium_type = "gemnasium-python"
  gemnasium_mr_url = "https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium-python/-/merge_requests/179+"

  content.each_line do |line|
    if line =~ /DS_REPORT_URL/
      if line =~ /gemnasium-maven/
        gemnasium_type = "gemnasium-maven"
        gemnasium_mr_url = "https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium-maven/-/merge_requests/195+"
      end

      line.gsub!(/gemnasium-(maven|python)/, "gemnasium")
    end
    modified_contents += line
  end

  branch_name = "point-#{gemnasium_type}-expectations-to-gemnasium-for-#{ref}-branch"

  mr_reviewer_username = "fcatteau"
  # mr_reviewer_username = "willmeek"
  # mr_reviewer_username = "gonzoyumo"
  # mr_reviewer_username = "adamcohen"

  mr_author_username = "adamcohen"
  mr_title = "Use gemnasium instead of #{gemnasium_type} in DS_REPORT_URL"

  uri = URI.parse("https://gitlab.com/api/v4/projects/#{project_id}/merge_requests?state=opened&author_username=#{mr_author_username}&search=#{mr_title}&source_branch=#{branch_name}")
  request = Net::HTTP::Get.new(uri)
  request["Private-Token"] = private_token

  req_options = {
    use_ssl: uri.scheme == "https",
  }

  response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
    http.request(request)
  end

  if response.code != "200"
    puts "Error while attempting to fetch MR details from URL #{uri}: #{response.body}"
    exit(1)
  end

  parsed_response = JSON.parse(response.body)
  if !parsed_response.empty?
    puts "An open MR with the title '#{mr_title}' already exists for project, skipping MR creation."
    next
  end

  timestamp = Time.now.to_i

  res = Gitlab.create_branch(project_id, branch_name, ref)
  puts "Successfully created branch: "
  puts JSON.pretty_generate({ name: res["name"], commit_title: res["commit"]["title"], author_name: res["commit"]["author_name"] })
  puts

  commit_message = "Set DS_REPORT_URL expectation to use gemnasium"
  commit_options = { :author_name => "Adam Cohen", :author_email => "acohen@gitlab.com" }
  res = Gitlab.edit_file(project_id, filename, branch_name, modified_contents, commit_message, commit_options)
  puts "Successfully created commit: "
  puts JSON.pretty_generate({ file_path: res["file_path"], branch: res["branch"] })
  puts

  mr_options = {
    :source_branch => branch_name,
    :target_branch => ref,
    :description => %Q(
## What does this MR do?

Now that #{gemnasium_mr_url} has been merged, and all related code has been moved to the [gemnasium](https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium) project, we need to update the `DS_REPORT_URL` and replace references to `#{gemnasium_type}` with `gemnasium`.

## What are the relevant issue numbers?

https://gitlab.com/gitlab-org/gitlab/-/issues/363591+

/milestone %"15.1"
/assign_reviewer @#{mr_reviewer_username}
/assign @#{mr_author_username}
  ),
    :labels => "maintenance::pipelines, Enterprise Edition, GitLab Ultimate, backend, Category:Dependency Scanning, backend, devops::secure, group::composition analysis, section::sec, test, type::maintenance",
    :remove_source_branch => true,
    :squash => true
  }

  res = Gitlab.create_merge_request(project_id, mr_title, mr_options)

  mr_link = %Q(
- [ ] [#{project["path"].sub("gitlab-org/security-products/tests/", "")}](#{File.join("https://gitlab.com/", project["path"])})
   - [ ] [#{project["branch"]}](#{res["web_url"]})
  )

  puts "Successfully created MR: "
  puts JSON.pretty_generate(
         {
           merge_request_url: res["web_url"],
           id: res["id"],
           iid: res["iid"],
           project_id: res["project_id"],
           title: res["title"]
         })

  puts "MR link: \n #{mr_link}"
end
