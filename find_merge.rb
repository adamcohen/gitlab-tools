#!/usr/bin/env ruby

require 'rubygems'

if ARGV.length == 0
  puts "Error: you must provide a SHA to lookup"
  exit 1
end

sha = ARGV[0]

merge = `merge=$(sh -c 'commit=#{sha} && branch=${1:-HEAD} && \
    (git rev-list $commit..$branch --ancestry-path | \
    cat -n; git rev-list $commit..$branch --first-parent | cat -n) | \
    sort -k2 -s | uniq -f1 -d | sort -n | tail -1 | cut -f2') && \
    [ -n \"$merge\" ] && git show $merge`

# merge = `git show-merge #{sha}`

/See merge request (?<merge_url>.*)/ =~ merge

url = "https://gitlab.com/#{merge_url.gsub('!', '/-/merge_requests/')}"
puts url

puts "Copying url to clipboard"
`echo '#{url}' | pbcopy`
