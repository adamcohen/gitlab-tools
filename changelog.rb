#!/usr/bin/env ruby

require 'rubygems'
require 'open-uri'
require 'gitlab'
require 'semantic'
require 'optparse'

GITLAB_ACCESS_TOKEN_PATH = File.join(__dir__, ".gitlab-access-token")

# fetch project id using the API. Could also just scrape the page, but that won't allow us
# to fetch confidential issues:
def fetch_project_id(project_path)
  escaped_project_path = CGI::escape(project_path)
  url = "https://gitlab.com/api/v4/projects/#{escaped_project_path}"

  project_details = JSON.parse(URI.open(url).read)
  project_details["id"]
end

unless File.exists?(GITLAB_ACCESS_TOKEN_PATH)
  puts "Error: you must provide a GitLab private token."
  exit 1
end

Gitlab.configure do |config|
  # API endpoint URL, default: ENV["GITLAB_API_ENDPOINT"] and falls back to ENV["CI_API_V4_URL"]
  config.endpoint       = "https://gitlab.com/api/v4"
  # user"s private token or OAuth2 access token, default: ENV["GITLAB_API_PRIVATE_TOKEN"]
  config.private_token  = File.read(GITLAB_ACCESS_TOKEN_PATH).strip
end

Options = Struct.new(:version_type, :message)

class Parser
  def self.parse(options)
    # default_version type is patch
    args = Options.new("patch")

    opt_parser = OptionParser.new do |opts|
      opts.banner = "Usage: changelog.rb [options] <message>"

      opts.on("--type [TYPE]", [:major, :minor, :patch],
                "Select version type to increment (major, minor, patch)") do |t|
        args.version_type = t
      end

      opts.on("-h", "--help", "Prints this help") do
        puts opts
        exit
      end
    end

    opt_parser.parse!(options)
    return args
  end
end

options = Parser.parse(ARGV)
changelog_message = ARGV.join(" ")

git_remote_output = %x{git remote -v}

if $? != 0
  puts "Error: you must be in a git repo to use this command."
  exit
end

line = git_remote_output.each_line.find { |line| line =~ /origin/}

m = line.match(/origin\tgit@gitlab\.com:(?<project_path>.*)\.git/)

project_id = fetch_project_id(m[:project_path])

merge_requests = Gitlab.merge_requests(project_id, { per_page: 1 })
current_max_mr_id = merge_requests.first["iid"]
new_mr_id = current_max_mr_id.to_int + 1

version_string = ""
File.readlines('CHANGELOG.md').each do |line|
  if /## v(?<version_string>\d*\.\d*\.\d)/ =~ line
    break
  end
end

version = Semantic::Version.new(version_string)

new_version = version.increment!(options.version_type)

output = "## v#{new_version.to_s}\n- #{changelog_message} (!#{new_mr_id})"

puts "New changelog entry: \n #{output}\n\n"
puts "Copying changelog entry to clipboard"
`echo '#{output}' | pbcopy`
