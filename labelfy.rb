#!/usr/bin/env ruby

require 'rubygems'
require 'open-uri'
require 'gitlab'

if ARGV.length == 0
  puts "Error: you must provide a GitLab issue/mr URL to fetch."
  exit 1
end

gitlab_access_token_path = File.join(File.dirname(File.realpath(__FILE__)), ".gitlab-access-token")

unless File.exists?(gitlab_access_token_path)
  puts "Error: you must provide a GitLab private token."
  exit 1
end

Gitlab.configure do |config|
 # API endpoint URL, default: ENV["GITLAB_API_ENDPOINT"] and falls back to ENV["CI_API_V4_URL"]
  config.endpoint       = "https://gitlab.com/api/v4"
  # user"s private token or OAuth2 access token, default: ENV["GITLAB_API_PRIVATE_TOKEN"]
  config.private_token  = File.read(gitlab_access_token_path).strip
end

url = ARGV[0]

url_prefix="https:\/\/gitlab\.com\/(?<group_name>.*)\/(?<project_name>.*)\/-"
labels = []

# fetch project id using the API. Could also just scrape the page, but that won't allow us
# to fetch confidential issues
def fetch_project_id(group_prefix, project_name)
  project_path = CGI::escape("#{group_prefix}/#{project_name}")
  url = "https://gitlab.com/api/v4/projects/#{project_path}"

  project_details = JSON.parse(URI.open(url).read)
  project_details["id"]
end

if m = url.match(/#{url_prefix}\/merge_requests\/(?<mr_id>[0-9]+)/)
  project_id = fetch_project_id(m[:group_name], m[:project_name])
  mr = Gitlab.merge_request(project_id, m[:mr_id])
  labels = mr.labels
elsif m = url.match(/#{url_prefix}\/issues\/(?<issue_id>[0-9]+)/)
  project_id = fetch_project_id(m[:group_name], m[:project_name])
  issue = Gitlab.issue(project_id, m[:issue_id])
  labels = issue.labels
end

output = %|/label ~"#{labels.join('" ~"')}"|
puts "Extracted the following labels: \n #{output}\n\n"
puts "Copying labels to clipboard"
`echo '#{output}' | pbcopy`
