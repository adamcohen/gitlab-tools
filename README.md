# GitLab Tools

This repo contains a collection of command line tools and scripts useful for GitLab developers.

# Requirements

Create a file called `.gitlab-access-token` in the same directory as these scripts, which contains your personal access token on a single line.

This personal access token will be used for certain scripts that require API access.

# Scripts

1. `find_merge.rb <SHA>`

   Shows the merge request that contains the commit with the given `SHA`. This command must be executed in the directory where the change with the given `SHA` was committed, for example:

   ```shell
   $ git clone git@gitlab.com:gitlab-org/security-products/analyzers/gemnasium.git
   $ cd gemnasium
   $ find_merge.rb e02fde83c4c0df526d84d9004c3433a396869151

   https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium/-/merge_requests/138
   Copying url to clipboard
   ```

1. `labelfy.rb <MR or issue URL>`

   Fetches labels for the given MR or issue, for example:

   ```shell
   $ labelfy.rb https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium/-/merge_requests/138

   Extracted the following labels:
    /label ~"Category:Dependency Scanning" ~"backend" ~"devops::secure" ~"group::composition analysis" ~"maintenance::refactor" ~"section::sec" ~"type::maintenance"

   Copying labels to clipboard
   ```

1. `changelog.rb --type [TYPE] <message>`

   Copies a new changelog entry to the clipboard with a bumped changelog version value and MR ID. This can be pasted directly into the `CHANGELOG.md` file. For example:

   ```shell
   $ git clone git@gitlab.com:gitlab-org/security-products/analyzers/gemnasium.git
   $ cd gemnasium

   $ changelog.rb --type patch fix some bug

   New changelog entry:
    ## v2.37.1
   - fix some bug (!312)

   Copying changelog entry to clipboard
   ```
